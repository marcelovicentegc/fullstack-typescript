# 🈵 fullstack-typescript

## 📦 General features

- Hot module and live reload
- File upload ready

## 💻 Client-side features

- UI framework:

  - **React**

- State management:

  - **MobX**

- GraphQL client:

  - **Apollo Client**

- Testing framework:
  - **Enzyme**

## 🖥 Server-side features

- HTTP server:

  - **Express**

- GraphQL server:

  - **Apollo Server**

- Database:

  - **PostgreSQL**

- Object-relational mapping framework:

  - **TypeORM**

- Data query and manipulation framework:

  - **GraphQL**

## 🌱 Directions

1. Clone this repo: `git clone https://github.com/marcelovicentegc/fullstack-typescript.git`
2. Change directory: `cd fullstack-typescript`
3. Install dependencies: `yarn install` or `npm install`
4. Create a Postgres database and set your credentials on a `ormconfig.json` file, similar to `ormconfig.example.json`
5. Run the application: `yarn start` or `npm run start`

### Environment variables

- Client TCP: `CLIENT_TCP`
- Server TCP: `SERVER_TCP`
- Environment: `ENV`

By default, the client runs on port `4000` and the server on port `8080`.
